au BufNewFile,BufRead *.doxygen set filetype=doxygen
au BufNewFile,BufRead *.c       set filetype=c.doxygen
au BufNewFile,BufRead *.cpp     set filetype=cpp.doxygen
au BufNewFile,BufRead *.h       set filetype=cpp.doxygen
au BufNewFile,BufRead *.hpp     set filetype=cpp.doxygen


au FileType python      setlocal tabstop=8 shiftwidth=4 softtabstop=4 expandtab
au FileType cpp         setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab
au FileType javascript  setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab
au FileType htmljinja   setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab
au FileType jinja       setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab
au FileType css         setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab
au FileType html        setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab
au FileType htmldjango  setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab
au FileType eruby       setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab
au FileType ruby        setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab
au FileType gitconfig   setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab
au FileType erlang      setlocal tabstop=8 shiftwidth=4 softtabstop=4 expandtab
au FileType ocaml       setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab
au FileType clojure     setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab
au FileType cmake       setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab
au FileType vim         setlocal tabstop=8 shiftwidth=4 softtabstop=4 expandtab
au FileType sh          setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab


autocmd FileType c,cpp,java,scala let b:comment_leader = '// '
autocmd FileType sh,ruby,python   let b:comment_leader = '# '
autocmd FileType vim              let b:comment_leader = '" '

noremap <silent> <leader>cc :<C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
noremap <silent> <leader>cu :<C-B>silent <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:nohlsearch<CR>
