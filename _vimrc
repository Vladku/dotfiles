scriptencoding utf-8
set encoding=utf-8

" This is useless, because when a `vimrc` or `gvimrc` file exists, Vim will use the Vim defaults, otherwise it will use
" the Vi defaults.
" But it is 'better safe than sorry'.
set nocompatible

if &term == 'xterm'
    set t_Co=256
endif

" disable arrow keys
inoremap <Up>    <NOP>
inoremap <Down>  <NOP>
inoremap <Left>  <NOP>
inoremap <Right> <NOP>
noremap  <Up>    <NOP>
noremap  <Down>  <NOP>
noremap  <Left>  <NOP>
noremap  <Right> <NOP>

" Stuff needed by pathogen
filetype off
call pathogen#infect()
call pathogen#helptags()

" Map leader to comma `,`.
let mapleader=","

" Allow Vim to manage multiple buffers effectively:
" 1. The current buffer can be put to the background without writing to disk.
" 2. When a background buffer becomes current again, marks and undo-history are remembered.
set hidden

" Make line numbers to be absolute by default, unless moving around.
:set relativenumber
autocmd InsertEnter * silent! :set norelativenumber
autocmd InsertLeave,BufNewFile,VimEnter * silent! :set relativenumber

" By default, Vim only remembers the last 20 commands and search patterns entered.
" Increase history size.
set history=1000

" By default, pressing <TAB> in command mode will choose the first possible completion with no indication of how many
" others there might be. The following configuration lets you see what your other options are.
set wildmenu

" Make the completion behave similarly to a shell, i.e. complete only up to the point of ambiguity (while still showing
" what options are).
set wildmode=list:longest

" Vim's |'wildignore'| setting is used to determine which files should be
" excluded from listings. This is a comma-separated list of glob patterns.
" It defaults to the empty string, but common settings include "*.o,*.obj"
" (to exclude object files) or ".git,.svn" (to exclude SCM metadata
" directories). For example:
"
" :set wildignore+=*.o,*.obj,.git
"
" A pattern such as "vendor/rails/**" would exclude all files and
" subdirectories inside the "vendor/rails" directory (relative to
" directory Command-T starts in)
set wildignore+=*.pyc,*.swap,*~,.git,3rd-party,*.xcodeproj,Build,*.png,*.jpg,*.tiff,*.ttf,build,external,target,node_modules

" These two options, when set together, will make /-style searches case-sensitive only if there is a capital letter in
" the search expression. *-style searches continue to be consistently case-sensitive.
set ignorecase smartcase

" A running gvim will always have a window title, but when vim runs within an xterm, by default it inherits the
" terminal’s current title.
" This gives e.g. | page.html (~) - VIM |.
set title

" Fold method based on indent. Type za to open and close fold
set foldmethod=indent
set foldlevel=99
" Don't fold by default
set nofoldenable

"set incsearch
set showmatch
set nowrap
set textwidth=120

" Move between windows using Ctrl+{j,k,l,h}
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" Also we can split windows using:
" _Vertical Split :      Ctrl + w + v
" _Horizontal Split:     Ctrl + w + s
" _Close current window: Ctrl + w + q
" _Create new window:    Ctrl + w + n

" Disable swap files
set noswapfile
set nobackup
set nowb

map <leader>y "*y
nnoremap <leader><leader> <c-^>

set completeopt=menuone,longest
set pumheight=10

syntax on
filetype on
filetype plugin indent on

set number
set modeline

set backspace=indent,eol,start

" Turn off visual bell in terminal
set vb t_vb=

set background=dark
colorscheme onedark
set cc=120 " Display right margin at 120 column

" Display trailing tabs and spaces
set list listchars=tab:\ \ ,trail:·
" Load custom settings
so ~/.vim/settings.vim

